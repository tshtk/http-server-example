package org.example.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Apartment {
    private long id;
    private String owner;
    private long created;
    private String numberOfRooms;
    private int price;
    private int area;
    private boolean balcony;
    private boolean loggia;
    private int floor;
    private int floorsInHouse;

    public Apartment(final Apartment apartment) {
        this.id = apartment.id;
        this.owner = apartment.owner;
        this.created = apartment.created;
        this.numberOfRooms = apartment.numberOfRooms;
        this.price = apartment.price;
        this.area = apartment.area;
        this.balcony = apartment.balcony;
        this.loggia = apartment.loggia;
        this.floor = apartment.floor;
        this.floorsInHouse = apartment.floorsInHouse;
    }
}
