package org.example;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.dto.ApartmentRQ;
import org.example.handler.ApartmentHandler;
import org.example.manager.ApartmentManager;
import org.example.framework.auth.CertificatePrincipal;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.middleware.AnonAuthMiddleware;
import org.example.framework.middleware.SecretAuthNMiddleware;
import org.example.framework.middleware.X509AuthNMiddleware;
import org.example.framework.util.Maps;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;

@Slf4j
public class Main {
    public static void main(String[] args) throws IOException {
        String password = new String(Files.readAllBytes(Paths.get("password.txt")));
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", password);
        System.setProperty("javax.net.ssl.trustStore", "web-certs/truststore.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", password);

        final ApartmentManager manager = new ApartmentManager();

        manager.create(new ApartmentRQ(
                "three rooms",
                6_000_000,
                84_5,
                true,
                true,
                10,
                10), new CertificatePrincipal("vasya"));

        manager.create(new ApartmentRQ(
                "one room",
                2_600_000,
                38_5,
                true,
                false,
                3,
                9), new CertificatePrincipal("petya"));



        final Gson gson = new Gson();
        final ApartmentHandler apartmentHandler = new ApartmentHandler(gson, manager);

        String getById = "^/apartments/(?<apartmentId>[0-9]{1,9})$";
        String update = "^/apartments/(?<apartmentId>[0-9]{1,9})/update$";
        String delete = "^/apartments/(?<apartmentId>[0-9]{1,9})/delete$";
        final Server server = Server.builder()
                .middleware(new X509AuthNMiddleware())
                .middleware(new SecretAuthNMiddleware())
                .middleware(new AnonAuthMiddleware())
                .routes(
                        Maps.of(
                                Pattern.compile("^/apartments$"), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getAll,
                                        HttpMethods.POST, apartmentHandler::create
                                ),
                                Pattern.compile(getById), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getById
                                ),
                                Pattern.compile(update), Maps.of(
                                        HttpMethods.POST, apartmentHandler::update
                                ),
                                Pattern.compile(delete), Maps.of(
                                        HttpMethods.POST, apartmentHandler::delete
                                )
                        )
                )
                .build();

        final int port = 8443;
        try {
            server.serveHTTPS(port);
        } catch (IOException e) {
            log.error("can't serve", e);
        }
    }
}
